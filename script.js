// экранировнаие преднозначено для того что б избежать спец. символы, когда не хотим, чтобы они имели свое особое значение 
// ткк в некоторых случаях символы по типу $, ', +, % - могут иметь специальное предназначение, и для того чтоб избежть
// использования их по назначению, а просто в тектсте, то мы должны их "экранировать"
function createNewUser(firstName, lastName, birthDay) {
  function validateDate() {
    const arrDate = birthDay.split(".");
    const date = new Date(arrDate[2], arrDate[1], arrDate[0]);
    while((date.getFullYear() != arrDate[2]) && (date.getMonth() != arrDate[1]) && (date.getDate() != arrDate[0])) {
      alert("Введена некорректная дата!");
      newUser.birthDay =  prompt("enter birth date AGAIN")
      return false;
    } 
    if ((date.getFullYear() == arrDate[2]) && (date.getMonth() == arrDate[1]) && (date.getDate() == arrDate[0])){
      return true;
    }
  }
    const newUser = {
      birthDay,
      getLogin: function() {
        return "login: " + this.firstName.trim().charAt(0).toLowerCase() + this.lastName.trim().toLowerCase()   //.trim для того чтоб избежать ошибок с пробелами
      }, 
      getAge: function() {
        return new Date().getFullYear() - new Date(this.birthDay.split('.').reverse().join(".")).getFullYear()
      },
      getPassword: function(){
        return "password: " + this.firstName.trim().charAt(0).toUpperCase() + this.lastName.trim().toLowerCase() + new Date(this.birthDay.split('.').reverse().join(".")).getFullYear()
      },
      setFirstName: function(newFirstName) {
        Object.defineProperties(newUser, {
          "firstName": {
            value: newFirstName
          }
        })
      },
      setLastName: function(newLastName) {
        Object.defineProperties(newUser, {
          "lastName": {
            value: newLastName
          }
        })
      }
    }
    Object.defineProperties(newUser, {
      "firstName": {
        value: firstName,
        enumerable: true,
        writable: false,
        configurable: true
      },
      "lastName": {
        value: lastName,
        enumerable: true,
        writable: false,
        configurable: true
      },
    })
    validateDate()
    newUser.firstName = "TEST_NAME";
    newUser.lastName = "TEST_LASTNAME"; 
    // newUser.setFirstName("George")
    // newUser.setLastName("Harrison") //- проверка на смену значений 
    console.log(newUser)
    console.log(newUser.getLogin())
    console.log(newUser.getAge())
    console.log(newUser.getPassword())
    return newUser
  }
  createNewUser(prompt("enter your name"), prompt("enter your last name"), prompt("enter birth date"))


